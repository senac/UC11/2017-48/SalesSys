package br.com.senac.salessys.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.salessys.R;
import br.com.senac.salessys.model.Cliente;

public class MainActivity extends AppCompatActivity {




    private List<Cliente> listaCliente = new ArrayList<>();

    private ArrayAdapter<Cliente> adapter ;





    public static final String CLIENTE = "cliente" ;
    public static final int NOVO = 1  ;
    public static final int EDITAR = 2  ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaCliente.add(new Cliente(1, "Daniel Santiago" , null , null ,null, null,null,null , null )) ;
        listaCliente.add(new Cliente(2, "jose Santiago" , null , null ,null, null,null,null , null )) ;
        listaCliente.add(new Cliente(3 , "Miguel Santiago" , null , null ,null, null,null,null , null )) ;

        ListView listView = findViewById(R.id.listaCliente) ;

        adapter = new ArrayAdapter<Cliente>(this
                ,android.R.layout.simple_list_item_1
                , listaCliente) ;

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posicao, long posicaoItemAdapter) {

                Cliente cliente  = adapter.getItem(posicao) ;
                Intent intent = new Intent(MainActivity.this , ClienteActivity.class) ;
                intent.putExtra(CLIENTE , cliente) ;
                startActivityForResult(intent , EDITAR);

            }
        });




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.novo:
                Log.i("#MENU", "Clicou em novo ");
                break;

            case R.id.sobre:
                Log.i("#MENU", "Clicou em sobre ");
                break;


        }


        return true;
    }


    public void novo(MenuItem item) {

        Intent intent = new Intent(MainActivity.this, ClienteActivity.class);

        startActivityForResult(intent , NOVO);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == NOVO){

            if(resultCode == RESULT_OK){
                Cliente cliente = (Cliente)data.getSerializableExtra(ClienteActivity.CLIENTE) ;
                listaCliente.add(cliente);
                adapter.notifyDataSetChanged();
                Toast.makeText(this , "Nome:"+ listaCliente.size() , Toast.LENGTH_LONG).show();
            }else if(resultCode == RESULT_CANCELED){
                //dasdadasd
            }


        }else if(resultCode == EDITAR){
            Cliente cliente = (Cliente)data.getSerializableExtra(ClienteActivity.CLIENTE) ;

            for(int i = 0 ; i < listaCliente.size() ; i++){
                if(listaCliente.get(i).getId() == cliente.getId()){
                    listaCliente.set(i , cliente);
                }
            }

            adapter.notifyDataSetChanged();
        }


    }


































    public void sobre(MenuItem item) {

    }


}
